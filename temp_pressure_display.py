from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
from time import sleep
from PiicoDev_TMP117 import PiicoDev_TMP117
from PiicoDev_MS5637 import PiicoDev_MS5637
from PiicoDev_BME280 import PiicoDev_BME280
#from PiicoDev_Unified import sleep_ms
from time import sleep
import framebuf

# sleep_ms(100)

oled_width = 128
oled_height = 64

i2c = I2C(0, scl = Pin(9), sda = Pin(8), freq=200000)

oled = SSD1306_I2C(oled_width, oled_height, i2c)

def scroll_in_screen(screen):
  for i in range (0, oled_width+1, 4):
    for line in screen:
      oled.text(line[2], -oled_width+i, line[1])
    oled.show()
    if i!= oled_width:
      oled.fill(0)

def scroll_out_screen(speed):
  for i in range ((oled_width+1)/speed):
    for j in range (oled_height):
      oled.pixel(i, j, 0)
    oled.scroll(speed,0)
    oled.show()
    
def scroll_screen_in_out(screen):
  for i in range (0, (oled_width+1)*2, 1):
    for line in screen:
      oled.text(line[2], -oled_width+i, line[1])
    oled.show()
    if i!= oled_width:
      oled.fill(0)

def scroll_in_screen_v(screen):
  for i in range (0, (oled_height+1), 1):
    for line in screen:
      oled.text(line[2], line[0], -oled_height+i+line[1])
    oled.show()
    if i!= oled_height:
      oled.fill(0)

def scroll_out_screen_v(speed):
  for i in range ((oled_height+1)/speed):
    for j in range (oled_width):
      oled.pixel(j, i, 0)
    oled.scroll(0,speed)
    oled.show()

def scroll_screen_in_out_v(screen):
  for i in range (0, (oled_height*2+1), 1):
    for line in screen:
      oled.text(line[2], line[0], -oled_height+i+line[1])
    oled.show()
    if i!= oled_height:
      oled.fill(0)

def static_screen(screen):
    oled.fill(0)
    for line in screen:
      oled.text(line[2], line[0], line[1])
    oled.show()
      
tempSensor = PiicoDev_TMP117()
pressure = PiicoDev_MS5637()
envSensor = PiicoDev_BME280()
zeroAlt = envSensor.altitude()

while True:
    
    tempC = tempSensor.readTempC() # Celsius
    tempF = tempSensor.readTempF() # Farenheit
    tempK = tempSensor.readTempK() # Kelvin
    
    tempC, presPa, humRH = envSensor.values()
    pres_hPab = presPa / 100
    print(str(tempC)+" °C  " + str(pres_hPab)+" hPa  " + str(humRH)+" %RH")
    
    print("Celcsius: {0:.1f}".format(tempC))
    
    press_hPa = pressure.read_pressure()
    
    print("hPa: {0:.1f}".format(press_hPa))

    screen1_row1 = "Temperature"
    screen1_row2 = "Celcsius: {0:.1f}".format(tempC)
    screen1_row3 = "Farenheit: {0:.1f}".format(tempF)
    screen1_row4 = "Kelvin: {0:.1f}".format(tempK)

    screen1 = [[0, 0 , screen1_row1], [0, 16, screen1_row2], [0, 32, screen1_row3], [0, 48, screen1_row4]]

    screen2_row1 = "Pressure"
    screen2_row2 = "hPa: {0:.1f}".format(press_hPa)
    screen2_row3 = ""
    screen2_row4 = ""

    screen2 = [[0, 0 , screen2_row1], [0, 16, screen2_row2], [0, 32, screen2_row3], [0,48, screen2_row4]]

    screen3_row1 = "BME280"
    screen3_row2 = "Temp: " + str(tempC)
    screen3_row3 = "Pressure: " + str(pres_hPab)
    screen3_row4 = "Humidity: " + str(humRH)

    screen3 = [[0, 0 , screen3_row1], [0, 16, screen3_row2], [0, 32, screen3_row3], [0,48, screen3_row4]]

#     scroll_in_screen(screen1)
#     scroll_out_screen(4)
#     scroll_in_screen(screen2)
#     scroll_out_screen(4)
#     scroll_screen_in_out(screen1)
#     scroll_screen_in_out(screen2)
#     scroll_screen_in_out(screen3)
#     scroll_in_screen_v(screen1)
#     scroll_out_screen_v(4)
#     scroll_in_screen_v(screen2)
#     scroll_out_screen_v(4)
#     scroll_screen_in_out_v(screen1)
#     scroll_screen_in_out_v(screen2)
#     scroll_screen_in_out_v(screen3)
    static_screen(screen1)
    sleep(3)
    static_screen(screen2)
    sleep(3)
    static_screen(screen3)
    sleep(3)

